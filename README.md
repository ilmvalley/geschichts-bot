![Logo](src/geschichts_bot/G.png)

Ein Chatbot, der im Geschichtsunterricht helfen soll geschrieben in Java 11.

Um ihn auszuführen brauchen sie auch Java 11.

## Probleme

[![Discord](https://img.shields.io/discord/501373450477240320?color=%237289da&label=discord-chat&logo=discord&style=for-the-badge)](https://discord.gg/xgXwTse)

Falls sie Probleme mit Geschichts-Bot haben können sie sich im Discord an die Entwickler wenden oder eine "Issue" im Bitbucket Projekt erstellen.

## Bauen

Nach jeden Beitrag zum Projekt wird der Geschichts-Bot in den Pipelines gebaut.
Das Fehler zu finden, bevor sie veröffentlicht werden.

Wenn eine Änderung in den master aufgenommen wird, wird automatisch ein neues Paket in den Downloads erzeugt.

Falls sie den Geshichts-Bot lieber selbst compilieren, folgt hier eine Anleitung:  
Zunächst brauchen sie eine JDK 11.  
Danach können sie eine Komandozeile öffenen und mit `cd` in den Ordner von Geschichts-Bot navigieren.  
Sobald sie im ordner von Geschichts-Bots Quelltext sind können sie folgende Befehle benutzen.

Windows:  
_Ausführen:_ `gradlew :run`  
_Bauen:_ `gradlew :build`

Linux/Mac OS:  
_Ausführen:_ `./gradlew :run`  
_Bauen:_ `./gradlew :build`

Nach dem Bauen, sollten die fertigen Pakete in `/build/distributions/` zu finden sein.

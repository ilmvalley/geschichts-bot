package ui;

import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.TrayIcon;
import geschichts_bot.Main;

public class TraySymbol extends TrayIcon {
	public TraySymbol() {
		super(Main.icon, "Geschichts-Bot");
        
        //build the pop up menu
        final PopupMenu popup = new PopupMenu();
        
        // Create a pop up menu components
        MenuItem openItem = new MenuItem("oeffnen");
        MenuItem exitItem = new MenuItem("beenden");
        
        //Add components to pop up menu
        popup.add(openItem);
        popup.add(exitItem);
        
        //set the pop-up menu of the tray to the created menu
        setPopupMenu(popup);

        setImageAutoSize(true);
        
        //open when the tray icon is double clicked
        addActionListener(Main.openListener);
        
        //open when open is clicked
        openItem.addActionListener(Main.openListener);
        
        //exit properly when exit is clicked
        exitItem.addActionListener(e -> {System.exit(0);});
	}
}

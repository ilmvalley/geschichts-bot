package ui;

import java.awt.FlowLayout;
import java.awt.Font;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import foxLog.threaded.AsyncLog;
import geschichts_bot.Main;
import geschichts_bot.Message;

public final class WritingCaption extends JPanel {

	private static final long serialVersionUID = -6345314975144732554L;
	
	/**
	 * messages to be written by the bot
	 */
	private List<String> messages = new LinkedList<>();
	
	/**
	 * a runnable for the writing animation
	 */
	private Runnable writing = new Runnable() {
		public synchronized void run() {
			//check if messages need to be written
			while (!messages.isEmpty()) {
				//get first message
				String text = messages.get(0);
				//do length of message / 2 times
				for (int i = 0; i < text.length() / 2 && messageIsFirst(text); i++) {
					//update label
					writingLabel.setText("schreibt"+dots());
					//wait 0.1 seconds
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						AsyncLog.warning("writing was interrupted", e);
						return;
					}
				}
				//if the message is still in the list
				if (messageIsFirst(text)) {
					//add the message
					Main.addMessage(new Message(text, false), false);
					//done writing
					messages.remove(text);
				}
			}
			//idle
			writingLabel.setText("");
		}
	};
	
	/**
	 * the current thread playing the writing animation
	 */
	private Thread writingThread = null;
	
	/**
	 * the number of dots shown
	 */
	private int dotsStep = 3;

	/**
	 * the big caption
	 */
	private static final JLabel captionLabel = new JLabel(Chat.bot_name);
	
	/**
	 * the smaller label next to the caption
	 */
	private static final JLabel writingLabel = new JLabel("");

	/**
	 * the singleton panel containing the labels
	 */
	private static WritingCaption singleCaption = null;

	/**
	 * the constructor for the panel
	 */
	private WritingCaption() {
		//build the panel itself
		super();
		//align left
		((FlowLayout) getLayout()).setAlignment(FlowLayout.LEFT);
		//align labels on the baseline
		//((FlowLayout) getLayout()).setAlignOnBaseline(true);
		//set the font for the caption
		captionLabel.setFont(new Font("writing font", Font.BOLD, 26));
		//add the caption
		add(captionLabel);
		//align the writing label
		writingLabel.setVerticalAlignment(JLabel.BOTTOM);
		//set the color for the writing label
		writingLabel.setForeground(Chat.bot_color);
		//add the writing label
		add(writingLabel);
	}
	
	public static WritingCaption getInstance() {
		if (singleCaption == null)
			singleCaption = new WritingCaption();
		return singleCaption;
	}
	
	/**
	 * calculate the number of dots for the animation
	 * @return
	 *   the dots as string
	 */
	private String dots() {
		//increase number of dots up to 3 then restart at 0
		dotsStep = (dotsStep  + 1) % 4;
		//construct return string
		String ret = "";
		//add the dots
		for (int i = 0; i < dotsStep; i++)
			ret += ".";
		//return the dots
		return ret;
	}

	/**
	 * check if the message still needs to be written
	 * @param text
	 *   the messages text
	 * @return
	 *   if the message is the first element in the list
	 */
	private boolean messageIsFirst(String text) {
		return !messages.isEmpty() && text.equals(messages.get(0));
	}

	/**
	 * start writing a message
	 * @param message
	 *   the messages text
	 */
	public synchronized void write(String message) {
		messages.add(message);
		if (writingThread == null || !writingThread.isAlive()) {
			writingThread = new Thread(writing);
			writingThread.start();
		}
	}
	
	/**
	 * cancel writing a message
	 * @param message
	 *   the messages text
	 */
	public void cancel(String message) {
		//might be unnessecary
		messages.remove(message);
	}
}

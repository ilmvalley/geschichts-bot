package ui;

import javax.swing.JLabel;

import java.awt.Graphics;
import java.awt.Color;

public class MessageBox extends JLabel {
	private static final long serialVersionUID = 3271464319655390203L;
	
	private Color color;
	
	public MessageBox(Color c) {
		super();
		color = c;
	}
	
	public MessageBox() {
		this(null);
	}

	public void paintComponent(Graphics g) {
		if (color != null) {
			g.setColor(color);
			g.fillRoundRect(0, 0, getWidth(), getHeight(), 15, 15);
		}
		
		super.paintComponent(g);
	}
	
	public Color getBoxColor() {
		return color;
	}

	public void setBoxColor(Color c) {
		color = c;
	}

}

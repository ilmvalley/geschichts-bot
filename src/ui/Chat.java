package ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import foxLog.threaded.AsyncLog;
import geschichts_bot.Main;
import geschichts_bot.Message;
import interpreter.MessageProcessor;

/**
 * the chat window
 */
public class Chat extends JFrame {
	
	private static final long serialVersionUID = -6892727620468367015L;
	
	/**
	 * the visual container for the messages
	 */
	private final JPanel msgCont = new JPanel();
	
	/**
	 * a scroll pane for scrolling through the messages
	 */
	private final JScrollPane contScroller = new JScrollPane(msgCont);
	
	/**
	 * the text field where the user can write his messages
	 */
	private final JTextField writeField = new JTextField();
	
	public static final String bot_name = "Geschichts-Bot";
	public static final String user_name = "Benutzer";
	public static final Color bot_color = Color.decode("#7d2953");
	public static final Color user_color = Color.decode("#2cabfe");
	
	/**
	 * the button to send messages
	 */
	private final JButton buttonWrite = new JButton("senden");

	private final ActionListener writeMessage = e -> {
		//log the message send action
		AsyncLog.verbose(e.toString());
		//get the message
		String text = e.getActionCommand();
		//don't write empty messages
		if (text.isEmpty())
			return;
		//create the message object
		Message msg = new Message(text,true);
		//add the message to the chat
		Main.addMessage(msg, false);
		//process the message
		MessageProcessor.getInstance().msg(msg);
		//empty the write field
		writeField.setText("");
	};
	
	public final Runnable scrollDown = new Runnable() {
		public synchronized void run() {
			int value;
			JScrollBar vScrollBar = contScroller.getVerticalScrollBar();
			//JScrollBar hScrollBar = contScroller.getHorizontalScrollBar();
			//FIXME: value can't go up to the maximum
			while ((value = vScrollBar.getValue()) < vScrollBar.getMaximum()) {
				if (vScrollBar.getValueIsAdjusting())
					break;
				/*if (!hScrollBar.getValueIsAdjusting() && hScrollBar.getValue() != hScrollBar.getMaximum()/2) {
					int diff = hScrollBar.getMaximum()/2 - value;
					double nextValue = Math.ceil(value + Math.signum(diff) + Math.pow(diff/500,2));
					hScrollBar.setValue((int) nextValue);
				}*/
				int diff = vScrollBar.getMaximum() - value;
				double nextValue = Math.ceil(value + Math.pow(diff/400,3));
				vScrollBar.setValue((int) nextValue);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					AsyncLog.info("scrolling was interrupted");
					return;
				}
			}
		}
	};

	private Thread scrolling_thread = null;

	/**
	 * build the chat window
	 */
	public Chat() {
		//build a frame and give it a title
		super("geschichts bot");
		//set icon
		setIconImage(new ImageIcon(Main.class.getResource("G.png")).getImage());
		//build a layout for the contents
		GridBagLayout gridbag = new GridBagLayout();
		//properties for the contents
		GridBagConstraints gbc = new GridBagConstraints();
		//set the new layout
		setLayout(gridbag);
		
		msgCont.setLayout(new BoxLayout(msgCont,BoxLayout.Y_AXIS));
		msgCont.add(Box.createVerticalGlue());
		msgCont.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				updateMsgCont(true);
			}
		});
		
		//increase the size of the small components to resize h and v
		gbc.fill = GridBagConstraints.BOTH;
		//take up space with priority of 1
		gbc.weightx = 1;
		//take only the height needed
		gbc.weighty = 0;
		//use all the width available
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		//get the WritingCaption
		JPanel writingCaption = WritingCaption.getInstance();
		//set properties for the label
		gridbag.setConstraints(writingCaption, gbc);
		//add it to the frame
		add(writingCaption);
		
		//add the messages to the message container
		for (Message message : Main.messages) {
			appendMessage(message);
		}
		//take up space with priority of 1
		gbc.weighty = 1;
		gbc.weightx = 1;
		//use all the width available
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		//set properties for the scrolling pane
		gridbag.setConstraints(contScroller, gbc);
		//add it to the frame
		add(contScroller);
		
		//add the writing listener to the text field
		writeField.addActionListener(writeMessage);
		//take only the height needed
		gbc.weighty = 0;
		//use width relative to other components
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		//set the properties for the text field
		gridbag.setConstraints(writeField, gbc);
		//add it to the frame
		add(writeField);
		
		//add an action listener for writing to the button
		buttonWrite.addActionListener(arg0 -> writeMessage.actionPerformed(new ActionEvent(buttonWrite, ActionEvent.ACTION_PERFORMED, writeField.getText(),System.currentTimeMillis(),0)));
		//take only the width needed
		gbc.weightx = 0;
		//use up the width left
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		//set the properties for the button
		gridbag.setConstraints(buttonWrite, gbc);
		//add it to the frame
		add(buttonWrite);
		
		//pack the frame and its contents
		pack();
		//set window maximized
		setExtendedState(MAXIMIZED_BOTH);
		//destroy if closed
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//send close it if closed
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (scrolling_thread != null)
					scrolling_thread.interrupt();
				Main.addMessage(new Message("::leave",true),true);
				if (Main.noTrayIcon())
					System.exit(0);
			}
		});
		contScroller.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				abortScrolling();
			}
		});
		msgCont.setSize(new Dimension(getWidth(), msgCont.getPreferredSize().height));
		msgCont.addContainerListener(new ContainerListener() {
			public void componentAdded(ContainerEvent e) {
					updateMsgCont(false);
			}

			public void componentRemoved(ContainerEvent e) {
				updateMsgCont(false);
			}
		});
		//show to the user
		setVisible(true);
	}
	
	public void userJoin() {
		//send user entry
		Message msg = new Message("::join", true);
		Main.addMessage(msg, true);
		//process the message
		MessageProcessor.getInstance().msg(msg);
	}
	
	private void updateMsgCont(boolean adjusting) {
		//show new messages
		msgCont.revalidate();
		//resize message container
		int preferredHeight = 20;
		for (Component c : msgCont.getComponents()) {
			preferredHeight += c.getPreferredSize().height;
		}
		msgCont.setPreferredSize(new Dimension(getWidth()-30, preferredHeight));
		//scroll down as far as possible
		if (scrolling_thread != null)
			scrolling_thread.interrupt();
		if (!adjusting) {
			scrolling_thread  = new Thread(scrollDown);
			scrolling_thread.setName("scrolling thread");
			scrolling_thread.start();
		}
	}

	/**
	 * add a message to the message container
	 * @param msg
	 *   the message to be added
	 */
	public void appendMessage(Message msg) {
		//get the text
		String processedText = msg.text;
		//don't do anything if it's empty
		if (processedText.isBlank())
			return;
		MessageBox msgBox = new MessageBox();
		ImageIcon userimage = null;
		
		if (processedText.startsWith("::")) {
			//replace command by it's description
			//FIXME: does not find patterns with parameters
			processedText = MessageProcessor.getInstance().getDescription(processedText, msg.fromUser);
			//don't do anything if it's empty
			if (processedText.isBlank())
				return;
			//commands are gray and italic
			msgBox.setBoxColor(Color.GRAY);
			msgBox.setHorizontalAlignment(JLabel.CENTER);
			msgBox.setAlignmentX(.5f);
			processedText ="<i>" + processedText + "</i>";
		} else {
			//fetch the color
			msgBox.setBoxColor(msg.fromUser ? user_color : bot_color);
			msgBox.setComponentOrientation(msg.fromUser ? ComponentOrientation.RIGHT_TO_LEFT : ComponentOrientation.LEFT_TO_RIGHT);
			//msgBox.setHorizontalAlignment(msg.fromUser ? JLabel.RIGHT : JLabel.LEFT);
			msgBox.setAlignmentX(msg.fromUser ? 0 : 1);
			userimage = new ImageIcon(msg.fromUser ? Main.class.getResource("user.jpeg") : Main.class.getResource("G_small.png")); 
		}
		
		msgBox.setText("<html>"+processedText+"</html>");
		msgBox.setIcon(userimage);
		msgBox.setForeground(Color.WHITE);
		msgCont.add(msgBox);
	}

	public void abortScrolling() {
		if (scrolling_thread != null)
			scrolling_thread.interrupt();
	}
}

package interpreter;

public class Latch {
	private volatile boolean value;

	public boolean state() {
		return value;
	}

	public void set() {
		value = true;
	}

	public void reset() {
		value = false;
	}
}

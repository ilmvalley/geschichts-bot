package interpreter.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.regex.Matcher;

import foxLog.threaded.AsyncLog;

public class Intent extends Entity {
	
	public Intent(String name) {
		super(name);
	}
	
	public static final Set<String> nominative_articles = Set.of("dieses","diese","die","der","das");
	public static final Set<String> dative_articles = Set.of("diesem","diesen","dem","den","der");

	protected Set<String> questions = new LinkedHashSet<>();
	protected Set<String[]> split = new LinkedHashSet<>();
	protected Set<String[]> splitQuestions = new LinkedHashSet<>();
	protected Set<String> aliases = new LinkedHashSet<>();
	protected Set<IntentProperty> properties = new LinkedHashSet<>();
	
	public static Set<Intent> readIntents(BufferedReader reader) throws IOException {
		Set<Intent> intents = new LinkedHashSet<>();
		Intent currentIntent = null;
		//read everything
		String line;
		while ((line = reader.readLine()) != null) {
			if (line.isBlank())
				continue;
			//get first char
			char lineStart = line.charAt(0);
			//get rest
			String lineContent = line.substring(1);
			//check which beginning
			switch (lineStart) {
			case '/':
				AsyncLog.verbose("comment: " + lineContent);
				break;
			case '$':
				String intentName = line;
				if (line.contains(" "))
					intentName = line.split(" ")[0];
				currentIntent = new Intent(intentName);
				intents.add(currentIntent);
				break;
			case '!':
				currentIntent.addProperty(lineContent);
				break;
			case '-':
				if (currentIntent == null)
					AsyncLog.error("intent file started with pattern");
				else
					currentIntent.addReplacement(lineContent);
				break;
			case '~':
				if (currentIntent == null)
					AsyncLog.error("intent file started with pattern");
				else
					currentIntent.addQuestion(lineContent);
				break;
			case '+':
				if (currentIntent == null)
					AsyncLog.error("intent file started with pattern");
				else
					currentIntent.addSplit(lineContent);
				break;
			case ':':
				if (currentIntent == null)
					AsyncLog.error("intent file started with pattern");
				else
					currentIntent.addSplitQuestion(lineContent);
				break;
			case '%':
				if (currentIntent == null)
					AsyncLog.error("intent file started with alias");
				else
					currentIntent.addAlias(lineContent);
				break;
			default:
				//something that we don't know yet
				AsyncLog.error("unrecognized beginning " + lineStart);
				break;
			}
		}
		return intents;
	}
	
	protected void addProperty(String propertyName) {
		IntentProperty prop;
		try {
			prop = IntentProperty.valueOf(propertyName); 
		} catch (IllegalArgumentException e) {
			AsyncLog.error(propertyName + " is not an IntentProperty", e);
			return;
		}
		if (prop == null) {
			AsyncLog.error("Got null instead of exception.",
					new NullPointerException(propertyName + " is not an IntentProperty"));
			return;
		}
		
		properties.add(prop);
	}

	public void addQuestion(String text) {
		questions.add(text);
	}
	
	public void addSplit(String text) {
		split.add(text.split("\\s\\*\\s"));
	}

	public void addSplitQuestion(String text) {
		splitQuestions.add(text.split("\\s\\*\\s"));
	}
	
	public void addAlias(String intent) {
		aliases.add(intent);
	}

	public String replace(String text) {
		String ret = text;
		String[] splitOccurence;
		
		while ((splitOccurence = findLongestSplitMatching(splitQuestions, ret, true)) != null) {
			if (splitOccurence.length == 0)
				continue;
			ret = ret.replace(splitOccurence[0], identifier);
			for (String occurence : splitOccurence)
				ret = ret.replace(occurence, "");
			ret = ret.replace("?", "");
		}
		while ((splitOccurence = findLongestSplitMatching(split, ret, true)) != null) {
			if (splitOccurence.length == 0)
				continue;
			ret = ret.replace(splitOccurence[0], identifier);
			for (String occurence : splitOccurence)
				ret = ret.replace(occurence, "");
		}
		
		String occurence;
		while ((occurence = findLongestMatching(questions, ret, true)) != null) {
			ret = ret.replace(occurence, identifier);
			ret = ret.replace("?", "");
		}
		while ((occurence = findLongestMatching(replacements, ret, true)) != null)
			ret = ret.replace(occurence, identifier);
		
		if (properties.contains(IntentProperty.nominative_articles)) {
			for (String article : nominative_articles) {
				ret = ret.replace(identifier + " " + article + " ", identifier + " ");
			}
		}
		if (properties.contains(IntentProperty.dative_articles)) {
			for (String article : dative_articles) {
				ret = ret.replace(identifier + " " + article + " ", identifier + " ");
			}
		}
		
		return ret;
	}
	
	public Set<String> replaceAliases(String text) {
		Set<String> ret = new LinkedHashSet<>();
		
		//TODO: add all possible results
		for (String intid : aliases) {
			ret.add(text.replace(intid, identifier));
		}
		
		return ret;
	}

	public String mix(String text) {
		String ret = text;
		//TODO: add ability to mix questions
		while (ret.contains(identifier)) {
			ret = ret.replaceFirst(Matcher.quoteReplacement(identifier), Matcher.quoteReplacement(randomReplacement()));
		}
		return ret;
	}

	protected String randomReplacement() {
		//TODO: return question replacements too
		int len = replacements.size();
		if (len == 0)
			throw new NoSuchElementException("replacements for "+identifier+" were empty");
		int i = 0;
		Iterator<String> it = replacements.iterator();
		while (it.hasNext()) {
			if (Math.random() <= ((double) (i+++1) / (double) len))
				return it.next();
			else
				it.next();
		}
		throw new NoSuchElementException("random ran out of choices");
	}
	
	protected String[] findLongestSplitMatching(Set<String[]> splitReplacements, String text, boolean fullWords) {
		String[] found = null;
		int foundLength = -1;
		String lowerText = text.toLowerCase();
		for (String[] splitReplacement : splitReplacements) {
			boolean matched = true;
			int currentLength = 0;
			for (String part : splitReplacement) {
				currentLength += part.length();
				String lowerPart = part.toLowerCase();
				if (!contains(lowerText, lowerPart, fullWords))
					matched = false;
			}
			if (matched && (found == null || currentLength > foundLength)) {
				found = new String[splitReplacement.length];
				int i = 0;
				for (String part : splitReplacement) {
					int replacementIndex = lowerText.indexOf(part.toLowerCase());
					found[i++] = text.substring(replacementIndex, replacementIndex + part.length());
				}
				foundLength = currentLength;
			}
		}
		return found;
	}
}

package interpreter.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;

import foxLog.threaded.AsyncLog;

public class Entity {
	public final String identifier;
	
	protected Set<String> replacements = new LinkedHashSet<>();
	
	public Entity(String name) {
		identifier = name;
	}
	
	protected void addReplacement(String text) {
		//TODO: add a filter pattern here
		replacements.add(text);
	}

	public static Set<Entity> readEntities(BufferedReader reader) throws IOException {
		Set<Entity> entities = new LinkedHashSet<>();
		Entity currentEntity = null;
		//read everything
		String line;
		while ((line = reader.readLine()) != null) {
			if (line.isBlank())
				continue;
			//get first char
			char lineStart = line.charAt(0);
			//get rest
			String lineContent = line.substring(1);
			//check which beginning
			switch (lineStart) {
			case '&':
				if (!line.endsWith(";"))
					AsyncLog.warning("unterminated entitiy: " + line);
				String entityName = line;
				currentEntity = new Entity(entityName);
				entities.add(currentEntity);
				break;
			case '-':
				if (currentEntity == null)
					AsyncLog.error("enity file started with pattern");
				else
					currentEntity.addReplacement(lineContent);
				break;
			default:
				//something that we don't know yet
				AsyncLog.error("unrecognized beginning " + lineStart);
				break;
			}
		}
		try {
			reader.close();
		} catch (IOException e) {
			AsyncLog.error("couldn't close reader", e);
		}
		return entities;
	}

	public String replace(String text) {
		String ret = text;
		String occurence;
		while ((occurence = findLongestMatching(replacements, ret, false)) != null)
			ret = ret.replace(occurence, identifier);
		return ret;
	}
	
	protected static String findLongestMatching(Set<String> replacements, String text, boolean fullWords) {
		String found = null;
		String lowerText = text.toLowerCase();
		for (String replacement : replacements) {
			String lowerReplacement = replacement.toLowerCase();
			if (contains(lowerText, lowerReplacement, fullWords)) {
				if (found == null || replacement.length() > found.length()) {
					int replacementIndex = lowerText.indexOf(lowerReplacement);
					found = text.substring(replacementIndex, replacementIndex + replacement.length());
				}
			}
		}
		return found;
	}
	
	protected static boolean contains(String text, String chunk, boolean fullWords) {
		if (!text.contains(chunk))
			return false;
		if (text.equals(chunk))
			return true;
		if (!fullWords)
			return true;
		if (text.startsWith(chunk))
			return text.matches(Matcher.quoteReplacement(chunk) + "\\b.*");
		else if (text.endsWith(chunk))
			return text.matches(".*\\b" + Matcher.quoteReplacement(chunk));
		else {
			int chunkIndex = text.indexOf(chunk); 
			return text.substring(chunkIndex-1, chunkIndex + chunk.length() + 1).matches(".\\b" + Matcher.quoteReplacement(chunk) + "\\b.");
		}
	}
}

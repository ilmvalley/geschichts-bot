package interpreter.filter;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import foxLog.threaded.AsyncLog;
import geschichts_bot.ResourceLoader;
import geschichts_bot.ResourceLoader.ResourceNotFoundException;

public final class WordFilter {
	
	/**
	 * the word filter singleton
	 */
	public static WordFilter singleFilter = null;
	
	/**
	 * the words that can be left out without harm to the sentences meaning
	 */
	private Set<WordInSentence> meaninglessWords = new LinkedHashSet<>();

	private Set<Intent> intents = new LinkedHashSet<>();

	private Set<Entity> entities = new LinkedHashSet<>();
	
	/**
	 * the constructor initializing the WordFilter
	 */
	protected WordFilter() {
		try {
			loadRecursive("filters");
		} catch (IOException e) {
			AsyncLog.error("couldn't load WordFilter resources", e);
		}
	}
	
	public static WordFilter getInstance() {
		if (singleFilter == null)
			singleFilter = new WordFilter();
		return singleFilter;
	}
	
	/**
	 * loads a word filter file into the word filter
	 * @param file
	 *   the file or folder to be loaded
	 * @throws ResourceNotFoundException 
	 * @throws IOException
	 *   errors that occur while loading
	 */
	private void loadRecursive(String file) throws ResourceNotFoundException {
		ResourceLoader resourceLoader = ResourceLoader.getInstance();
		//check if we got a directory
		if (resourceLoader.isDirectory(file, WordFilter.class)) {
			//ensure separator at end
			final String path = file + (!file.endsWith("/") ? '/' : "");
			//get folder contents
			resourceLoader.forEachInFolder(file, e -> {
				try {
					//load file in current folder
					loadRecursive(path + e);
				} catch (IOException e1) {
					//oh no!
					AsyncLog.error("error while trying to load folder: \"" + e + '"', e1);
				}
			},WordFilter.class);
		} else {
			//read the file
			try {
				loadFilterFile(new BufferedReader(resourceLoader.getReader(file, WordFilter.class)));
			} catch (IOException e) {
				AsyncLog.error("error while trying to load filter file: "+ file, e);
			}
		}
	}
	
	/**
	 * load a file into the word filter
	 * @param reader
	 *   the reader for the file
	 * @throws IOException
	 *   errors that occur while reading
	 */
	private void loadFilterFile(BufferedReader reader) throws IOException {
		//get the first line
		String type = reader.readLine();
		//if nothing is there
		if (type == null) {
			throw new EOFException("reader for AAS file starts with null");
		}
		//check if its a word filter file
		if (!type.startsWith("?WordFilter")) {
			//nope
			AsyncLog.warning('"' + type + "\" is not a valid type line for a WordFilter file");
			//don't even try to parse it
			return;
		}
		//OK, got one
		type = type.substring("?WordFilter ".length());
		//check what kind of file it is
		switch (type) {
		case "meaningless":
			//meaningless words
			readMeaningless(reader);
			break;
		case "intents":
			//intents
			intents.addAll(Intent.readIntents(reader));
			break;
		case "entities":
			//entities for special characters
			entities.addAll(Entity.readEntities(reader));
			break;
		default:
			//something we don't know yet
			AsyncLog.warning("WordFilter file of type \"" + type + "\" is not recognized");
			break;
		}
	}

	private void readMeaningless(BufferedReader reader) throws IOException {
		//read everything
		for (String line = reader.readLine(); line!=null; line = reader.readLine()) {
			if (line.isBlank())
				continue;
			//get first char
			char lineStart = line.charAt(0);
			//get rest
			String lineContent = line.substring(1);
			//check which beginning
			switch (lineStart) {
			case '/':
				//comments
				AsyncLog.verbose("comment: "+ lineContent);
				break;
			case '+':
				//sentence start
				meaninglessWords.add(new WordInSentence(lineContent,LocationInSentence.Start));
				break;
			case '-':
				//sentence end
				meaninglessWords.add(new WordInSentence(lineContent,LocationInSentence.End));
				break;
			case '*':
				//anywhere
				meaninglessWords.add(new WordInSentence(lineContent,LocationInSentence.Anywhere));
				break;
			default:
				//something that we don't know yet
				AsyncLog.error("unrecognized beginning " + lineStart);
				break;
			}
		}
		try {
			reader.close();
		} catch (IOException e) {
			AsyncLog.error("couldn't close reader", e);
		}
	}

	/**
	 * reduce the patterns contents to the most complex form possible
	 * @param base
	 *   the pattern to be processed
	 * @return
	 *   the processed pattern with intents, without meaningless
	 */
	public String filterPattern(String base) {
		AsyncLog.verbose("filtering pattern: " + base);
		String ret = base.toLowerCase();
		
		//perhaps split sentences
		ret = filterEntities(ret);
		ret = filterMeaningless(ret);
		ret = filterIntents(ret);
		
		if (!ret.equalsIgnoreCase(base))
			AsyncLog.warning("filterPattern:\n\"" + base + "\"\n-----filtered to-----\n\"" + ret + '"');
		
		ret = filterPunctuation(ret);
		
		return ret;
	}
	
	public String filterMessage(String msg) {
		AsyncLog.verbose("filtering message: " + msg);
		String ret = msg.toLowerCase();
		
		//perhaps split sentences
		ret = filterEntities(ret);
		ret = filterMeaningless(ret);
		ret = filterIntents(ret);
		ret = filterPunctuation(ret);
		
		return ret;
	}
	
	public Set<String> replaceAliases(String msg) {
		Set<String> ret = new ConcurrentSkipListSet<>();
		ret.add(msg);
		//TODO: replace aliases parallel and get all possible constellations
		for (Intent intent : intents) {
			for (String fmsg : ret) {
				ret.addAll(intent.replaceAliases(fmsg));
			}
		}
		return ret;
	}

	public String filterAnswer(String ans) {
		AsyncLog.verbose("filtering answer: " + ans);
		String ret = ans;
		
		ret = filterEntities(ret);
		ret = filterIntents(ret);
		
		return ret;
	}

	public String mixAnswer(String ans) {
		AsyncLog.verbose("mixing answer: " + ans);
		String ret = ans;
		
		//ret = filterEntities(ret);
		ret = mixIntets(ret);
		
		return ret;
	}
	
	private String filterPunctuation(String text) {
		String ret = text;
		if (ret.endsWith("."))
			ret = ret.substring(0,ret.length()-1);
		
		if (ret.contains(".")) {
			//later recognize more accurately and return messages as array
			AsyncLog.info("multiple sentences detected in text: " + ret);
		}
		
		return ret;
	}
	
	private String filterMeaningless(String text) {
		String ret = text;
		boolean found = true;
		while (found) {
			found = false;
			for (WordInSentence meaningless : meaninglessWords) {
				switch (meaningless.location) {
				case Start:
					if (ret.startsWith(meaningless.word)) {
						ret = ret.substring(meaningless.word.length());
						found = true;
					}
					break;
				case Anywhere:
					if (ret.contains(meaningless.word)) {
						ret = ret.replaceAll(meaningless.word, "");
						found = true;
					}
					break;
				case End:
					if (ret.endsWith(meaningless.word)) {
						ret = ret.substring(0, ret.length()-meaningless.word.length());
						found = true;
					}
					break;
				default:
					AsyncLog.error("unknown location in sentence: " + meaningless.location);
				}
			}
			ret = ret.strip();
		}
		return ret;
	}

	private String filterEntities(String msg) {
		String ret = msg;
		for (Entity entity : entities) {
			ret = ret.strip();
			ret = entity.replace(ret);
		}
		return ret;
	}

	private String filterIntents(String msg) {
		String ret = msg;
		for (Intent intent : intents) {
			ret = intent.replace(ret);
			ret = ret.strip();
		}
		return ret;
	}

	private String mixIntets(String msg) {
		String ret = msg;
		for (Intent intent : intents) {
			ret = intent.mix(ret);
		}
		return ret;
	}
}

package interpreter.filter;

public enum IntentProperty {
	/**
	 * ignore the given articles after this intent
	 */
	nominative_articles,
	/**
	 * ignore the given articles after this intent
	 */
	dative_articles;
}
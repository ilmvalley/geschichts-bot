package interpreter.checkers;

import java.time.LocalTime;
import java.util.Map;
import java.util.Set;

import geschichts_bot.Message;
import interpreter.Checker;

/**
 * a checker handling messages asking for the time
 */
public final class TimeChecker extends Checker {

	/**
	 * all patterns that ask for the time
	 */
	private static final Set<String> activationPatterns = Set.of("wie sp&auml;t ist es?","wie sp&auml;t es ist");
	
	/**
	 * build the checker
	 */
	public TimeChecker() {
		super(activationPatterns);
	}

	/**
	 * a processor answering with the current time
	 * @see interpreter.Checker#process(java.lang.String, java.util.LinkedHashMap)
	 */
	public boolean process(String pattern, Map<String, String> params, Message msg) {
		answer("Es ist "+LocalTime.now().toString());
		return true;
	}

}

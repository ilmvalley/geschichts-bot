package interpreter.checkers;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import foxLog.LogLevel;
import foxLog.threaded.AsyncLog;
import geschichts_bot.Main;
import geschichts_bot.Message;
import geschichts_bot.SingleInstanceFile;
import interpreter.CommandChecker;

public class SystemCommands extends CommandChecker {
	
	public SystemCommands() {
		super(Map.of("::exit", "", "::delete data", "warnung: inhalt sollte gel&ouml;scht sein", "::close", "", "::log %%level%%", "log Level ge&auml;ndert", "::log", "log Hilfe"),null);
	}
	
	public boolean process(String pattern, Map<String, String> params, Message msg) {
		switch (pattern) {
		case "::exit":
			System.exit(0);
			return true;
		case "::delete data":
			Main.messageStore.delete();
			SingleInstanceFile.delete();
			Main.exitWithoutSave();
			return true;
		case "::close":
			Main.addMessage(new Message("::leave",true),true);
			if (Main.noTrayIcon())
				System.exit(0);
			new Thread(Main.chat::dispose).start();
			return true;
		case "::log":
			answer("aktuelle Log level ist: " + AsyncLog.getLevel().name());
			List<String> levels = new LinkedList<>();
			for (LogLevel lv : LogLevel.values())
				levels.add(lv.name());
			answer("Es gibt die Log level: " + levels);
			return true;
		case "::log %%level%%":
			try {
				AsyncLog.setLevel(LogLevel.valueOf(params.get("level").toUpperCase()));
			} catch (IllegalArgumentException e) {
				AsyncLog.warning("user entered invalid log level", e);
				answer("Es gibt Log level \"" + params.get("level").toUpperCase() + "\" nicht. Mit \"::log\" kannst du nachsehen welche es gibt.");
			}
			return true;
		default:
			AsyncLog.warning("unknown system command: " + pattern);
			return false;
		}
	}

}

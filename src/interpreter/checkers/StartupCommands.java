package interpreter.checkers;

import java.util.List;
import java.util.Map;

import foxLog.threaded.AsyncLog;
import geschichts_bot.Message;
import interpreter.CommandChecker;

public class StartupCommands extends CommandChecker {
	
	private static final Map<String,String> userCommands = Map.of("::init", "Simulation des Beginns des Chatverlaufs", "::join", "Benutzer ist dem Chat beigetreten", "::leave", "Benutzer hat den Chat verlassen", "::not_found", "Fehler: Befehl nicht gefunden");
	private static final Map<String,String> botCommands = Map.of("::init", "Beginn des Chatverlaufs", "::join", "Bot ist dem Chat beigetreten", "::leave", "Bot hat den Chat verlassen", "::not_found", "Befehl nicht gefunden");
	private static final List<String> welcome = List.of("Hallo.","Wilkommen zur&uuml;ck!","Sch&ouml;n dich wiederzusehen.","Wie kann ich dir helfen?");
	private static final List<String> wat = List.of("Was?","Huch?");
	
	public StartupCommands() {
		super(userCommands,botCommands);
	}

	public boolean process(String pattern, Map<String, String> params, Message msg) {
		switch (pattern) {
		case "::init":
			answer("Hallo, ich bin Geschichts-Bot");
			answer("Ich bin hier, um dir einen Teil der deutschen Geschichte n&auml;her zu bringen.");
			break;
		case "::join":
			if (msg.fromUser)
				answerRandom(welcome,.5);
			break;
		case "::leave":
			break;
		case "::not_found":
			answerRandom(wat,.2);
			break;
		default:
			AsyncLog.warning("unknown startup command: "+pattern);
			return false;
		}
		return true;
	}

}

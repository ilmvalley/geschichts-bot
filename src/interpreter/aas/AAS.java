package interpreter.aas;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import foxLog.threaded.AsyncLog;
import geschichts_bot.Main;
import geschichts_bot.Message;
import geschichts_bot.ResourceLoader;
import interpreter.Checker;
import interpreter.Latch;
import interpreter.filter.WordFilter;

public final class AAS extends Checker {
	
	class Pattern {
		
		private class ParamAnswers {
			
			private Map<String,List<String>> params = new LinkedHashMap<>();
			
			private final List<String> answers = new LinkedList<>();

			public void addAnswer(String answer) {
				//might not match all filters
				answers.add(WordFilter.getInstance().filterAnswer(answer));
			}

			public void addParameter(String parameter, String value) {
				List<String> values = params.get(parameter);
				if (values == null) {
					values = new LinkedList<>();
					params.put(parameter, values);
				}
				values.add(value);
			}

			public boolean hasParams(Map<String, String> params2) {
				if (!params2.keySet().equals(params.keySet()))
					return false;
				return params2.entrySet().stream().allMatch((e) -> {
					List<String> values = params.get(e.getKey());
					if (values==null)
						return false;
					if (!values.contains(e.getValue()))
						return false;
					return true;
				});
			}
			
			public List<String> getAnswers() {
				return answers;
			}
			
			public String toString() {
				return this.getClass().getSimpleName() + "[params="+params+",answers="+answers+"]";
			}
			
		}

		public final Set<String> bases = new LinkedHashSet<>();
		
		private Set<ParamAnswers> params = new LinkedHashSet<>();
		
		private double chanceEmpty = 0;

		public Pattern(String base) {
			addBase(base);
		}

		public ParamAnswers addParameters() {
			ParamAnswers param = new ParamAnswers();
			params.add(param);
			return param;
		}

		public List<String> getAnswers(Map<String, String> params2) { 
			List<String> answers = new LinkedList<>();
			Latch found = new Latch();
			params.forEach(paramAnswers -> {
				if (paramAnswers.hasParams(params2)) {
					found.set();
					answers.addAll(paramAnswers.getAnswers());
				}
			});
			if (found.state())
				return answers;
			else
				return null;
		}
		
		public String toString() {
			return this.getClass().getSimpleName()+"[bases="+bases+",params="+params+"]";
		}

		public void addBase(String base) {
			bases.add(WordFilter.getInstance().filterPattern(base));
		}

		public double getChanceEmpty() {
			return chanceEmpty;
		}

		public void setChanceEmpty(double chance) {
			chanceEmpty = chance;
		}

	}

	private Set<Pattern> patterns = new LinkedHashSet<>();
	public static AAS singleAAS = null;
	private AAS() {
		super(null);
		AsyncLog.action("loading AAS folder");
		try {
			loadRecursive("patterns");
		} catch (IOException e) {
			AsyncLog.error("couldn't load AAS folder", e);
		}
		updatePatterns();
		//use this command to see a big message
		//in fact it's for listing all registered patterns
		addPattern("::aas list");
		AsyncLog.debug("AAS initialized");
	}
	
	public static AAS getInstance() {
		if (singleAAS == null)
			singleAAS = new AAS();
		return singleAAS;
	}
	
	private void updatePatterns() {
		patterns.forEach(e -> {
			addPatterns(e.bases);
		});
	}

	/**
	 * load all the AAS contents of a folder or file
	 * @param file
	 *   the folder/file to be loaded
	 * @throws IOException
	 *   if an error occurs while loading a file 
	 */
	private void loadRecursive(String file) throws IOException {
		ResourceLoader resourceLoader = ResourceLoader.getInstance();
		//check if we got a directory
		if (resourceLoader.isDirectory(file, AAS.class)) {
			//ensure separator at end
			final String path = file + (!file.endsWith("/") ? '/' : "");
			//get folder contents
			resourceLoader.forEachInFolder(file, el -> {
				try {
					//load file in current folder
					loadRecursive(path + el);
				} catch (IOException e) {
					//oh no!
					AsyncLog.error("error while trying to load folder: \"" + el + '"', e);
				}
			},AAS.class);
		} else {
			BufferedReader reader = new BufferedReader(resourceLoader.getReader(file, AAS.class));
			String line;
			line = reader.readLine();
			if (line == null) {
				throw new EOFException("reader for AAS file starts with null");
			}
			if (line.startsWith("?AAS")) {
				if (!file.endsWith(".txt"))
					AsyncLog.warning("filename \"" + file + "\" does not end with \".txt\"");
				AsyncLog.debug("loading AAS File " + line);
				readAASFile(reader);
				return;
			} else {
				AsyncLog.error("file does not start with \"?AAS\"");
			}
		}
	}

	private void readAASFile(BufferedReader reader) throws IOException {
		Pattern pattern = null;
		Pattern.ParamAnswers params = null;
		//read everything
		for (String line = reader.readLine(); line!=null; line = reader.readLine()) {
			if (line.isBlank())
				continue;
			//get first char
			char lineStart = line.charAt(0);
			//get rest
			String lineContent = line.substring(1);
			//check which beginning
			switch (lineStart) {
			case '/':
				AsyncLog.verbose("comment: " + lineContent);
				break;
			case '#':
				pattern = new Pattern(lineContent);
				patterns.add(pattern);
				break;
			case '+':
				pattern.addBase(lineContent);
				break;
			case '~':
				pattern.setChanceEmpty(Double.parseDouble(lineContent));
				break;
			case '*':
				AsyncLog.verbose("new param Set: " + lineContent);
				params = pattern.addParameters();
				break;
			case '-':
				params.addAnswer(lineContent);
				break;
			default:
				if (line.contains("=")) {
					String[] content = line.split("=");
					if (content.length > 2)
						AsyncLog.error("malformed parameter line " + line);
					if (content.length == 2)
						params.addParameter(content[0],content[1]);
					else
						params.addParameter(content[0], "");
				} else
					AsyncLog.error("unrecognized beginning " + lineStart);
				break;
			}
		}
		try {
			reader.close();
		} catch (IOException e) {
			AsyncLog.error("couldn't close reader", e);
		}
	}
	
	Set<Pattern> getPatterns() {
		return patterns;
	}

	public boolean process(String pattern, Map<String, String> params, Message msg) {
		boolean found = false;
		for (Pattern pat : patterns) {
			if (!pat.bases.contains(pattern))
				continue;
			AsyncLog.info("found AAS pattern with the right base");
			List<String> ans = pat.getAnswers(params);
			if (ans == null)
				continue;
			if (ans.isEmpty()) {
				found = true;
				continue;
			}
			AsyncLog.debug("found AAS pattern matching the message");
			answerRandom(ans, pat.getChanceEmpty());
			found = true;
		}
		if (pattern.equalsIgnoreCase("::aas list")) {
			AsyncLog.action("listing all registered AAS patterns");
			Main.addMessage(new Message("liste aller registrierten AAS patterns:",false),false);
			Main.addMessage(new Message(patterns.toString(), false), false);
			found = true;
		}
		return found;
	}
}

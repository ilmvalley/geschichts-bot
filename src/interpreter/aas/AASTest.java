package interpreter.aas;

import java.util.List;
import java.util.Map;

import foxLog.threaded.AsyncLog;
import interpreter.Latch;
import interpreter.filter.WordFilter;
import testing.TestCase;

public class AASTest extends TestCase {
	
	public boolean findAASPattern(String pattern, Map<String, String> params) {
		String filteredPattern = WordFilter.getInstance().filterMessage(pattern);
		Latch search = new Latch();
		AAS.getInstance().getPatterns().forEach(pat -> {
			if (!pat.bases.contains(filteredPattern))
				return;
			AsyncLog.info("found AAS pattern with the right base");
			List<String> ans = pat.getAnswers(params);
			if (ans==null)
				return;
			if (ans.isEmpty())
				return;
			AsyncLog.debug("found AAS pattern matching the message");
			search.set();
		});
		return search.state();
	}
	
	public List<Runnable> initTests() {
		return List.of(
				() -> assertTrue(findAASPattern("AAS test",Map.of()),"found \"AAS test\"","\"AAS test\" didn't match"),
				() -> assertTrue(findAASPattern("AAS test",Map.of("test","test")),"found \"AAS test test\"","\"AAS test test\" didn't match"),
				() -> assertFalse(findAASPattern("AAS test",Map.of("test","not a test")),"\"AAS test not a test\" didn't match","\"AAS test not a test\" matched"));
	}

}

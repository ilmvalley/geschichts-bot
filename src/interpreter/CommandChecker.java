package interpreter;

import java.util.Map;
import java.util.Set;

public abstract class CommandChecker extends Checker {
	
	protected Map<String, String> userCommands;
	protected Map<String, String> botCommands;
	
	public CommandChecker(Map<String, String> userCommands, Map<String, String> botCommands) {
		this(userCommands,botCommands,null);
	}

	public CommandChecker(Map<String, String> userCommands, Map<String, String> botCommands, Set<Set<String>> aliases) {
		super(null, aliases);
		this.userCommands = userCommands;
		this.botCommands = botCommands;
		updatePatternSet();
	}
	
	//TODO: check in check whether it is the user or the bot
	
	protected void updatePatternSet() {
		clearPatterns();
		if (userCommands != null)
			userCommands.keySet().forEach(this::addPattern);
		if (botCommands != null)
			botCommands.keySet().forEach(this::addPattern);
	}
	
	public String getDescription(String pattern, boolean fromUser) {
		if (fromUser)
			if (userCommands == null)
				return null;
			else
				return userCommands.get(pattern);
		else
			if (botCommands == null)
				return null;
			else
			return botCommands.get(pattern);
	}
}

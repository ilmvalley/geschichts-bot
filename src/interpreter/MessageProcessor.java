package interpreter;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import foxLog.threaded.AsyncLog;
import geschichts_bot.Message;
import interpreter.aas.AAS;
import interpreter.checkers.StartupCommands;
import interpreter.checkers.SystemCommands;
import interpreter.checkers.TimeChecker;
import interpreter.filter.WordFilter;

/**
 * the processor that triggers all its checkers when it receives a message
 */
public final class MessageProcessor {
	
	/**
	 * the checkers that should be triggered when a message is received
	 */
	private List<Checker> checkers = new LinkedList<>();
	
	private static final double helpChance = 0.4;

	private static MessageProcessor singleProcessor = null;
	
	/**
	 * a method initializing the message processor,
	 * adds the checkers that should be checked by default
	 */
	private MessageProcessor() {
		AsyncLog.action("initializing the MessageProcessor");
		checkers.add(new SystemCommands());
		checkers.add(new StartupCommands());
		checkers.add(new TimeChecker());
		checkers.add(AAS.getInstance());
	}

	public static MessageProcessor getInstance() {
		if (singleProcessor == null)
			singleProcessor = new MessageProcessor();
		return singleProcessor;
	}
	
	/**
	 * let all checkers check the message
	 * @param msg
	 *   the message received
	 */
	public synchronized void msg(Message msg) {
		Latch search = new Latch();
		WordFilter wf = WordFilter.getInstance();
		String filtered = wf.filterMessage(msg.text);
		Set<String> results = wf.replaceAliases(filtered);
		
		ExecutorService pool = Executors.newCachedThreadPool();
		checkers.forEach((ch) -> {
			pool.execute(()-> {
				for (String text : results) {
					if (ch.check(text, msg)) {
						search.set();
						break;
					}
				}
			});
		});
		
		pool.shutdown();
		try {
			pool.awaitTermination(1, TimeUnit.HOURS);
		} catch (InterruptedException e) {
			AsyncLog.warning("interrupted while waiting for termination, killing pool",e);
			pool.shutdownNow();
			return;
		}
		
		if (!search.state()) {
			if (msg.text.startsWith("::")) {
				Checker.answer("::not_found");
			} else {
				//perhaps filter for the intents first
				String text = results.stream().findAny().orElse("");
				if (text.contains("$what_is") || text.contains("$time")) {
					Checker.answerRandom(List.of("Das wei&szlig; ich nicht.","Keine Ahnung..."));
				} else if (text.contains("$definition")) {
					Checker.answerRandom(List.of("Dazu habe ich leider keine Definition.","Ich wei&szlig; nicht wie ich das beschreiben soll."));
				} else {
					Checker.answerRandom(List.of("Das verstehe ich nicht.","Ich wei&szlig; nicht was du versuchst zu sagen."));
				}
				if (Math.random()<helpChance)
					Checker.answer("$helpTheDevs");
			}
		}
	}
	
	public String getDescription(String pattern, boolean fromUser) {
		for (Checker ch : checkers)
			if (ch instanceof CommandChecker) {
				String desc = ((CommandChecker) ch).getDescription(pattern,fromUser);
				if (desc != null)
					return desc;
			}
		return "Befehl " + pattern;
	}
}

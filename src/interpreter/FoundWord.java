package interpreter;

/**
 * a class that remembers a word and where it was found in a string
 */
public class FoundWord {

	public final String word;
	public final int idx;

	public FoundWord(String word, int idx) {
		this.word = word;
		this.idx = idx;
	}

}

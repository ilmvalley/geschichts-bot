package testing;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import foxLog.ConsoleStyle;
import foxLog.threaded.AsyncLog;
import geschichts_bot.Main;
import interpreter.aas.AASTest;
import testing.tests.*;

public final class TestRunner {
	
	private static final List<TestCase> cases = List.of(
			new SmokeTests(),
			new ObjectStoreTests(),
			new ResourceTests(),
			new FilterTests(),
			new CheckerTests(),
			new AASTest(),
			new MixerTests(),
			new CommandTests()
		);
	
	public static class TestResults {
		private static int failed = 0;
		private static int passed = 0;
		
		public static void addSuccess() {
			passed++;
		}

		public static void addFail() {
			failed++;
		}

		public static boolean noFails() {
			return failed == 0;
		}

		public static int getPassed() {
			return passed;
		}
		
		public static int getFailed() {
			return failed;
		}
	}
	
	/**
	 * prints the test results
	 * @return
	 *   whether all tests passed without errors
	 */
	public static boolean results() {
		AsyncLog.outPrintln(AsyncLog.stamp("testing result") + "all tests terminated");
		AsyncLog.log("tests passed: " + TestRunner.TestResults.getPassed(), ConsoleStyle.FOREGROUND_GREEN, System.out);
		AsyncLog.log("tests failed: " + TestRunner.TestResults.getFailed(), ConsoleStyle.FOREGROUND_RED, System.err);
		AsyncLog.printWarningCount();
		if ((!AsyncLog.printErrorCount()) && TestRunner.TestResults.noFails()) {
			AsyncLog.log("all tests passed", ConsoleStyle.FOREGROUND_GREEN, System.out);
			AsyncLog.flush();
			return true;
		} else {
			AsyncLog.log("tests failed and/or errors occurred", ConsoleStyle.FOREGROUND_RED, System.err);
			AsyncLog.flush();
			return false;
		}
	}
	
	public static void main(String[] args) {
		Main.checkLogArgs(args);
		
		try {
			AsyncLog.setLogFile(new File("test-log.txt"));
		} catch (FileNotFoundException e) {
			AsyncLog.error("Error while trying to set logfile",e);
		}
		
		Main.createSaveFolder();

		for (TestCase testCase : cases) {
			AsyncLog.log(AsyncLog.stamp("running test case") + testCase.getClass().getName(),
					ConsoleStyle.FOREGROUND_BLUE, System.out);
			List<Runnable> tests = testCase.initTests();
			for (Runnable test : tests) {
				
				String testName = test.getClass().getName();
				if (testName.contains("/"))
					testName = testName.substring(0,testName.lastIndexOf('/'));
				AsyncLog.log(AsyncLog.stamp("running test") + testName,
						ConsoleStyle.FOREGROUND_CYAN, System.out);
				test.run();
			}
		}
		
		Boolean success = results();
		AsyncLog.flush();
		if (success)
			System.exit(0);
		else
			System.exit(1);
	}

}

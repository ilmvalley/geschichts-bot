package testing;

import java.util.List;

import foxLog.ConsoleStyle;
import foxLog.threaded.AsyncLog;

public abstract class TestCase {

	public abstract List<Runnable> initTests();
	
	public static void pass(String passMessage) {
		AsyncLog.log(AsyncLog.stamp("test passed") + passMessage,
				ConsoleStyle.FOREGROUND_GREEN, System.out);
		TestRunner.TestResults.addSuccess();		
	}
	
	public static void fail(String failMessage) {
		AsyncLog.log(AsyncLog.stamp("test failed") + failMessage,
				ConsoleStyle.FOREGROUND_RED, System.err);
		TestRunner.TestResults.addFail();
	}
	
	public static void fail(String failMessage, Throwable e) {
		AsyncLog.log(AsyncLog.stamp("test failed") + failMessage, e,
				ConsoleStyle.FOREGROUND_RED, System.err);
		TestRunner.TestResults.addFail();
	}
	
	public static void assertTrue(boolean a) {
		assertTrue(a, "assertion passed (got true)", "assertion failed (got false)");
	}
	
	public static void assertTrue(boolean a, String passMessage, String failMessage) {
		if (a)
			pass(passMessage);
		else
			fail(failMessage);
	}
	
	public static void assertFalse(boolean a) {
		assertFalse(a, "assertion passed (got false)", "assertion failed (got true)");
	}
	
	public static void assertFalse(boolean a, String passMessage, String failMessage) {
		if (a)
			fail(failMessage);
		else
			pass(passMessage);
	}
	
	public static void assertNull(Object a) {
		assertNull(a, "assertion passed (got null)", "assertion failed (got " + a + ")");
	}
	
	public static void assertNull(Object a, String passMessage, String failMessage) {
		if (a == null)
			pass(passMessage);
		else
			fail(failMessage);
	}
	
	public static void assertEqual(Object a, Object b) {
		assertEqual(a, b, "assertion passed (equal)", "assertion failed (not equal)");
	}
	
	public static void assertEqual(Object a, Object b, String passMessage, String failMessage) {
		if (a == b)
			pass(passMessage);
		else if (a == null || b == null)
			fail(failMessage);
		else if (a.equals(b) && b.equals(a))
			pass(passMessage);
		else
			fail(failMessage);
	}
	
}

package testing.tests;

import java.util.List;

import testing.ParamTest;
import testing.TestCase;

public class SmokeTests extends TestCase {
	
	public static class ParamSmokeTest extends ParamTest<String> {
		
		private static final List<String> params = List.of("foo", "bar", "Hello world!");
		
		public ParamSmokeTest() {
			super(params);
		}
		
		public void test(String param) {
			assertTrue(params.contains(param),"got parameter: " + param,"got wrong parameter: " + param);
		}
	}
	
	public List<Runnable> initTests() {
		return List.of(
				() -> assertTrue(true, "true seems true", "true is not true?!"),
				() -> assertFalse(false, "false seems false", "false is not false?!"),
				() -> assertNull(null, "null seems null", "null is not null?!"),
				new ParamSmokeTest());
	}

}

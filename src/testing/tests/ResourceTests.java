package testing.tests;

import java.util.List;

import foxLog.threaded.AsyncLog;
import geschichts_bot.ResourceLoader;
import geschichts_bot.ResourceLoader.ResourceNotFoundException;
import testing.TestCase;

public class ResourceTests extends TestCase {
	
	public class DirectoryTest implements Runnable {
		public void run() {
			boolean isDir;
			try {
				isDir = ResourceLoader.getInstance().isDirectory("geschichts_bot", null);
			} catch (ResourceNotFoundException e) {
				fail("geschichts_bot was not found",e);
				return;
			}
			assertTrue(isDir,"/geschichts_bot/ is a folder","/geschichts_bot is not a folder");
		}
	}
	
	public class NotDirectoryTest implements Runnable {
		public void run() {
			boolean isDir;
			try {
				isDir = ResourceLoader.getInstance().isDirectory("G.png", ResourceLoader.class);
			} catch (ResourceNotFoundException e) {
				fail("test.txt was not found",e);
				return;
			}
			assertFalse(isDir,"G.png is not a folder","G.png is a folder");
		}
	}
	
	/* perhaps replace this by a new reading test
	public class TestFileTest extends Test {
		public void run() {
			Reader testReader = null;
			try {
				testReader = ResourceLoader.global.getReader("test.txt", ResourceLoader.class);
			} catch (IOException e) {
				fail("error while trying to fetch test.txt",e);
				return;
			}
			BufferedReader testBuf = new BufferedReader(testReader);
			try {
				assertEqual(testBuf.readLine(),"file for starting/testing the resource loader","first line of \"test.txt\" line matches","first line of \"test.txt\" is different");
				assertNull(testBuf.readLine(),"end of \"test.txt\" reached","end of \"test.txt\" not reached");
			} catch (IOException e) {
				Log.error("error while trying to read test.txt",e);
			}
		}
	}
	*/
	
	public class NotFileTest implements Runnable {
		public void run() {
			try {
				ResourceLoader.getInstance().isDirectory("something that doesn't exist", ResourceTests.class);
			} catch (ResourceNotFoundException e) {
				pass(e.getMessage());
				return;
			}
			fail("could get invalid resource file");
		}
	}
	
	public class ForEachTest implements Runnable {
		private boolean failed = false;
		private boolean found = false;
		
		public void run() {
			ResourceLoader.getInstance().forEachInFolder("testing", f -> {
				AsyncLog.verbose("testing folder contains: " + f);
				if (failed)
					return;
				found = true;
				if (f.contains("ResourceTets")) {
					fail("found content of a subfolder to testing: " + f);
					failed = true;
				}
			}, null);
			if (found) {
				if (!failed)
					pass("couldn't get subfolder contents of testing");
			} else
				fail("couldn't get folder contents of testing");
		}
	}
	
	public List<Runnable> initTests() {
		return List.of(
				new DirectoryTest(),
				new NotDirectoryTest(),
				new NotFileTest(),
				new ForEachTest()
			);
	}

}

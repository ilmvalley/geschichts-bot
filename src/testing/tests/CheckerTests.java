package testing.tests;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import geschichts_bot.Message;
import interpreter.Latch;
import interpreter.Checker;
import interpreter.filter.WordFilter;
import testing.ParamTest;
import testing.TestCase;

public class CheckerTests extends TestCase {
	
	private static Checker testC;
	
	public List<Runnable> initTests() {
		List<Runnable> tests = new LinkedList<>();
		Latch search = new Latch();
		testC = new Checker(Set.of("test %%num%%"), Set.of(Set.of("test2","test"))) {
			public boolean process(String pattern, Map<String, String> params, Message msg) {
				search.set();
				return true;
			}
		};
		tests.add(new ParamTest<String>("test 2", "test 3", "test2 test", "Test 1") {
			public void beforeTest() {
				search.reset();
			}
			
			public void test(String param) {
				testC.check(WordFilter.getInstance().filterMessage(param),null);
				assertTrue(search.state(), "\"" + param + "\" matched", "\"" + param + "\" didn't match");
			}
		});
		tests.add(new ParamTest<String>("test", "test3 3", "test3", "not a match") {
			public void beforeTest() {
				search.reset();
			}
			
			public void test(String param) {
				testC.check(param,null);
				assertFalse(search.state(), "\"" + param + "\" didn't match", "\"" + param + "\" matched");
			}
		});
		tests.add(new Runnable() {
			public void run() {
				search.reset();
				testC.addPattern("add");
				testC.check("add",null); //should pass
				assertTrue(search.state(), "added pattern matched","added pattern didn't match");
			}
		});
		tests.add(new Runnable() {
			public void run() {
				search.reset();
				testC.addPattern("add");
				testC.check("added",null); //shouldn't pass
				assertFalse(search.state(), "not added pattern didn't match","not added pattern matched");
			}
		});
		return tests;
	}
}

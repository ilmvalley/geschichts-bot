package testing.tests;

import java.util.List;

import interpreter.filter.WordFilter;
import testing.ParamTest;
import testing.TestCase;

public class FilterTests extends TestCase {
	
	public class equalTest extends ParamTest<String> {
		
		public equalTest() {
			super("test", "test ", "test.", "Test ", "chatbot, test.");
		}
		
		public void test(String param) {
			assertTrue(WordFilter.getInstance().filterMessage(param).equalsIgnoreCase("test"),
					"filtered \"" + param + "\" equals \"test\"",
					"filtered \"" + param + "\" isn't equal to \"test\"");
		}
	}

	public List<Runnable> initTests() {
		return List.of(() -> {
			assertTrue(("test foo test bar").replaceAll("test foo","test").equals("test test bar"),
					"replace works", "replacing test foo by test didn't work");
		}, new equalTest());
	}

}

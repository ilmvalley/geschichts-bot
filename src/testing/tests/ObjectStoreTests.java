package testing.tests;

import java.io.File;
import java.util.List;

import geschichts_bot.Main;
import geschichts_bot.ObjectStore;
import testing.TestCase;

public class ObjectStoreTests extends TestCase {
	
	private String storageFilePath = Main.saveFolder.getPath() + "/testing";
	private ObjectStore<String> store = new ObjectStore<>("testing");
	
	public class StorageTest implements Runnable {
		public void run() {
			List<String> testList = List.of("test","test2", "test3");
			store.saveList(testList);
			assertTrue(new File(storageFilePath).exists(),"storage file exists","storage file doesn't exist");
			assertTrue(store.loadVector(String.class).equals(testList),"list was saved and loaded","list couldn't be saved and/or loaded");
			store.delete();
			assertFalse(new File(storageFilePath).exists(),"storage file seems deleted", "storage file wasn't deleted");
		}
	}
	
	public List<Runnable> initTests() {
		return List.of(new StorageTest());
	}

}

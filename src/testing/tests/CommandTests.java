package testing.tests;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import geschichts_bot.Message;
import interpreter.CommandChecker;
import interpreter.Latch;
import testing.ParamTest;
import testing.TestCase;

public class CommandTests extends TestCase {

	@Override
	public List<Runnable> initTests() {
		List<Runnable> tests = new LinkedList<>();
		Latch search = new Latch();
		CommandChecker ch = new CommandChecker(Map.of("::test","test","::tests %%test2%%","param test"),Map.of("::test2","test2")) {
			@Override
			public boolean process(String pattern, Map<String, String> params, Message msg) {
				search.set();
				return true;
			}
			
		};
		tests.add(() -> {
			String desc = ch.getDescription("::test",true);
			assertTrue(desc.equals("test"),"command description was "+desc, "command description was "+desc);
		});
		/* FIXME: doesn't work
		tests.add(() -> {
			String desc = ch.getDescription("::tests 2",true);
			assertTrue(desc != null && desc.equals("param test"),"command description was "+desc, "command description was "+desc);
		});*/
		tests.add(() -> {
			String desc = ch.getDescription("::test",false);
			assertNull(desc,"command description was null", "command description was not null");
		});
		tests.add(() -> {
			String desc = ch.getDescription("::test2",false);
			assertTrue(desc.equals("test2"),"command description was "+desc, "command description was "+desc);
		});
		tests.add(new ParamTest<String>("::test","::tests 2") {
			public void beforeTest() {
				search.reset();
			}
			
			public void test(String param) {
				ch.check(param,null);
				assertTrue(search.state(),"user command " + param + " found","user command " + param + " not found");
			}			
		});
		tests.add(new ParamTest<String>("::crap","::test2 2") {
			public void beforeTest() {
				search.reset();
			}
			
			public void test(String param) {
				ch.check(param,null);
				assertFalse(search.state(),"invalid command not found","invalid command found");
			}			
		});
		CommandChecker ch2 = new CommandChecker(null,null) {
			public boolean process(String pattern, Map<String, String> params, Message msg) {
				return false;
			}
		};
		tests.add(() -> {
			String desc = ch2.getDescription("::test",false);
			assertNull(desc,"bot command description was null", "bot command description was "+desc);
		});
		tests.add(() -> {
			String desc = ch2.getDescription("::test",true);
			assertNull(desc,"user command description was null", "user command description was "+desc);
		});
		return tests;
	}

}

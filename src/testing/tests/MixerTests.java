package testing.tests;

import java.util.List;

import foxLog.threaded.AsyncLog;
import interpreter.filter.WordFilter;
import testing.ParamTest;
import testing.TestCase;

public class MixerTests extends TestCase {
	
	public static final List<String> testParams = List.of("Definiere den Test.","Warum testet man?", "Wann war der Test?");
	
	public class mixerTest extends ParamTest<String> {
		public mixerTest() {
			super(testParams);
		}
		
		public void test(String param) {
			WordFilter wf = WordFilter.getInstance();
			String firstFiltered = wf.filterAnswer(param);
			String mixed = wf.mixAnswer(firstFiltered);
			if (mixed.equals(firstFiltered))
				AsyncLog.warning("Filtered message: \"" + firstFiltered + "\" equals mixed messsage: \"" + mixed + "\"");
			String secondFiltered = wf.filterAnswer(mixed);
			assertEqual(firstFiltered, secondFiltered,
					"Mixed and then filtered message is stil: \""+firstFiltered + "\"",
					"Filtered message: \""+ firstFiltered + "\" is not equal to mixed and then filtered message: \"" + secondFiltered + "\"");
		}
	}
	
	public class DoubleFilterTest extends ParamTest<String> {
		public DoubleFilterTest() {
			super(testParams);
		}
		
		public void test(String param) {
			WordFilter wf = WordFilter.getInstance();
			String firstFiltered = wf.filterMessage(param);
			String secondFiltered = wf.filterMessage(param);
			assertEqual(firstFiltered, secondFiltered,
					"Double filtered message is stil: \"" + firstFiltered + "\"",
					"Filtered message: \""+ firstFiltered + "\" is not equal to double filtered message: \"" + secondFiltered + "\"");
		}
	}

	public List<Runnable> initTests() {
		return List.of(
				new mixerTest(),
				new DoubleFilterTest()
			);
	}

}

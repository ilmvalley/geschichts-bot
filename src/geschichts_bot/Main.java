package geschichts_bot;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.ProgressMonitor;

import foxLog.LogLevel;
import foxLog.MainHelper;
import foxLog.threaded.AsyncLog;
import interpreter.MessageProcessor;
import testing.TestRunner;
import ui.Chat;
import ui.TraySymbol;

public final class Main {

	public static final File saveFolder = new File(".geschichts-bot");

	/**
	 * the currently open chat window
	 */
	public static Chat chat = null;
	
	public static final Image icon = (new ImageIcon(Main.class.getResource("G.png"))).getImage();
	
	private static final Image new_icon = (new ImageIcon(Main.class.getResource("G_new.png"))).getImage();

	private static boolean new_message;

	/**
	 * the icon shown in the system tray
	 */
	private static TrayIcon trayIcon;
	
	private static SystemTray tray;
	
	private static Thread responseThread;
	
	/**
	 * all the messages that were sent
	 */
	public static Vector<Message> messages;
	
	/**
	 * the storage file for the messages
	 */
	public static ObjectStore<Message> messageStore = new ObjectStore<>("messages");

	public static ActionListener openListener = (e) -> {
    	if (new_message) {
    		if (trayIcon != null)
    			trayIcon.setImage(icon);
    		new_message = false;
    	}
        //check if the chat is showing
        if (chat == null || !chat.isShowing()) {
        	//open a new chat
        	chat = new Chat();
        	chat.userJoin();
        } else {
        	//move to front obviously
        	chat.toFront();
        }
    };

	private static Thread quitHook = new Thread("geschichts-bot_quitHook") {
		public void run() {
			AsyncLog.addLoggedThread(quitHook);
			AsyncLog.action("quitting");
			
			if (chat != null && chat.isShowing())
				Main.addMessage(new Message("::leave",true),true);

			addMessage(new Message("::leave",false),true);
			
			//save everything
			messageStore.saveList(messages);
			
			responseThread.interrupt();

		    //mark that everything has quit properly
			SingleInstanceFile.set("running", "false");

			//save it too
			SingleInstanceFile.save();
			
			AsyncLog.printWarningCount();
			AsyncLog.printErrorCount();
			AsyncLog.flush();
			AsyncLog.close();
		}
	};

	private static boolean withoutTrayIcon = false;
	
	private static final int STARTUP_STEPS = 200;
	private static int startupProgress;
	private static ProgressMonitor startupMonitor = new ProgressMonitor(null,"starte geschichts-bot","initialisierung...",0,STARTUP_STEPS);

	public static void addStartupProgress(int i) {
		startupMonitor.setProgress(startupProgress += i);
	}
	
	public static void startupMessage(String dispMsg, String logMsg) {
		AsyncLog.action("startup: " + logMsg);
		startupMonitor.setNote(dispMsg);
		addStartupProgress(10);
	}

	/**
	 * the running method for the application
	 * @param args
	 *   command line arguments:
     *     f or fullog for full logging,
     *     i or info for info logging,
     *     d or debug for debug logging,
     *     none for default logging
	 */
	public static void main(String[] args) {
		startupMessage("untersuchen der Umgebung","checking command line");
		checkLogArgs(args);
		if (MainHelper.hasOption(args, "t", "test"))
			TestRunner.main(args);
		if (MainHelper.hasOption(args, "T", "no-trayicon"))
			withoutTrayIcon = true;
		
		createSaveFolder();
		
		//load the property storage
		SingleInstanceFile.load();
		
		//check if already running
		notifyRunninig();

		//let's go
		startup();
		
		startupMessage("fertig","finished");
		
		if (startupProgress<STARTUP_STEPS)
			AsyncLog.warning("not enough startup steps: " + startupProgress + '/' + STARTUP_STEPS);
		if (startupProgress>STARTUP_STEPS)
			AsyncLog.warning("too many startup steps: " + startupProgress + '/' + STARTUP_STEPS);
		startupMonitor.close();
	}
	
	public static void checkLogArgs(String[] args) {
		if (MainHelper.hasOption(args, "f", "fulllog")) {
    		AsyncLog.setLevel(LogLevel.ALL); // for full logging
    		AsyncLog.debug("full logging enabled");
		} else if (MainHelper.hasOption(args, "i", "info")) {
    		AsyncLog.setLevel(LogLevel.INFO);
    		AsyncLog.debug("info logging enabled");
    	} else if (MainHelper.hasOption(args, "d", "debug")) {
    		AsyncLog.setLevel(LogLevel.DEBUG);
    		AsyncLog.debug("debug logging enabled");
    	} else
    		AsyncLog.setLevel(LogLevel.ACTION);
		if (MainHelper.hasOption(args, "C", "no-colorize"))
			AsyncLog.setOutputColorized(false);
		else if (MainHelper.hasOption(args, "c", "colorize"))
			AsyncLog.setOutputColorized(true);
	}

	public static void createSaveFolder() {
		if (!saveFolder.exists())
			saveFolder.mkdir();
	}

	/**
	 * tries to notify already running instances
	 * and closes if another instance responds
	 */
	private static void notifyRunninig() {
		if (SingleInstanceFile.get("running","false").equals("false")) {
			addStartupProgress(120);
			return;
		}
		SingleInstanceFile.set("try_open", "true");
		SingleInstanceFile.save();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			AsyncLog.warning("sleeping thread was interrupted", e);
		}
		SingleInstanceFile.load();
		startupMessage("überprüfe auf laufende Chatbots","checking for running chatbots");
		if (!SingleInstanceFile.get("open_ack", "false").equals("true")) {
			for (int i = 0; i<100; i++) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					AsyncLog.warning("sleeping thread was interrupted", e);
				}
				SingleInstanceFile.load();
				if (SingleInstanceFile.get("open_ack", "false").equals("true")) {
					break;
				}
				addStartupProgress(1);
			}
		} else
			addStartupProgress(100);
		if (SingleInstanceFile.get("open_ack", "false").equals("true")) {
			SingleInstanceFile.set("open_ack", "false");
			SingleInstanceFile.set("try_open", "false");
			SingleInstanceFile.save();
			AsyncLog.message("opened chat window of open instance");
			startupMonitor.close();
			System.exit(0);
		}
		startupMessage("scheinbar noch laufend","seems already running");
		//tell the user it hasn't closed
		int n = JOptionPane.showConfirmDialog(
				chat, "Geschichts-Bot läuft noch oder wurde nicht ordnungsgemäß beendet!\nDies kann Datenverlust verursachen. Geschichts-Bot ausführen?",
                "Geschichts-Bot unbeendet",
                JOptionPane.YES_NO_OPTION);
		if (n == JOptionPane.YES_OPTION) {
        	//probably hasn't closed properly
			SingleInstanceFile.set("try_open", "false");
			SingleInstanceFile.save();
            return;
		} else {
			startupMonitor.close();
			System.exit(0);
		}
	}

	/**
	 * the method for actually running the full application
	 */
	private static void startup() {
		startupMessage("erstelle Log Dateien","creating log files");
		// set the log file
		try {
			AsyncLog.setLogFile(new File("log.txt"));
		} catch (FileNotFoundException e) {
			AsyncLog.error("Error while trying to set logfile",e);
		}
		//make clear that it shouldn't be started another time
		SingleInstanceFile.set("running", "true");
		//write it to the file
		SingleInstanceFile.save();
		//startup heart beat thread
		responseThread = new Thread(Main::checkStorage,"geschichts-bot_responseDaemon");
		responseThread.setDaemon(true);
		responseThread.start();
		AsyncLog.addLoggedThread(responseThread);
		startupMessage("lade Nachrichten","loading messages");
		//load messages from the file
		messages = messageStore.loadVector(Message.class);
		startupMessage("baue Benachrichtigungssymbol","building tray icon");
		//build the tray icon
		makeTrayIcon();
		//add the quit hook
		Runtime.getRuntime().addShutdownHook(quitHook);
		startupMessage("sende start Nachricht","sending startup message");
		Message initMsg = null;
		if (messages.isEmpty()) {
			//create first message
			initMsg = new Message("::init", false);
			//send first message
			Main.addMessage(initMsg, true);
			//process it
			MessageProcessor.getInstance().msg(initMsg);
		}
		//send startup message
		Message jMsg = new Message("::join",false);
		Main.addMessage(jMsg,true);
		//process it
		MessageProcessor.getInstance().msg(jMsg);
		startupMessage("erstelle Chatfenster","creating chat");
		//create a chat window
		chat = new Chat();
		chat.userJoin();
	}

	private static void checkStorage() {
		while (true) {
			SingleInstanceFile.load();
			if (SingleInstanceFile.get("try_open", "false").equals("true")) {
				SingleInstanceFile.set("open_ack", "true");
				SingleInstanceFile.set("try_open", "false");
				SingleInstanceFile.save();
				if (openListener!= null)
					openListener.actionPerformed(new ActionEvent(responseThread, ActionEvent.ACTION_PERFORMED, null));
				AsyncLog.message("Chat window opened by another instance");
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				AsyncLog.action("response deamon was interrupted");
				return;
			}
		}
	}

	/**
	 * create the tray icon
	 */
	private static void makeTrayIcon() {
        if (withoutTrayIcon) {
        	AsyncLog.debug("started without tray icon");
        	trayIcon = null;
            return;
        }
        //Check the SystemTray support
        if (!SystemTray.isSupported()) {
            AsyncLog.warning("SystemTray is not supported!");
            trayIcon = null;
            return;
        }
		
        //get the system tray
        tray = SystemTray.getSystemTray();
        
    	trayIcon = new TraySymbol();
        try {
        	//add the icon to the tray
			tray.add(trayIcon);
		} catch (AWTException e) {
			//couldn't add
			AsyncLog.error("Error while trying to add the tray icon",e);
			trayIcon = null;
			return;
		}
	}

	public static boolean noTrayIcon() {
		return trayIcon==null;
	}

	public static void exitWithoutSave() {
		if (chat != null)
			chat.abortScrolling();
		
		Runtime.getRuntime().removeShutdownHook(quitHook);
		responseThread.interrupt();
		
	    //last message
		AsyncLog.action("exit");
		AsyncLog.flush();
		AsyncLog.close();
		System.exit(0);
	}

	/**
	 * send a message
	 * @param text
	 *   the raw text of the message to be added
	 * @param fromUser
	 *   whether the message is one that the user wrote
	 */
	public static void addMessage(Message msg,boolean quiet) {
		//yay, received some
		AsyncLog.verbose("new message");
		//add the message to the list
		messages.add(msg);
		if (chat != null && chat.isShowing()) {
			//add the message to the chat window
			chat.appendMessage(msg);
		} else if (!quiet) {
			//notify the user
			trayIcon.displayMessage("Nachricht erhalten", msg.text, TrayIcon.MessageType.NONE);
			//change tray icon
			trayIcon.setImage(new_icon);
			new_message = true;
		}
		//spit it out in the log
		AsyncLog.verbose(msg.text);
	}

}

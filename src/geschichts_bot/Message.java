package geschichts_bot;

import java.io.Serializable;

/**
 * a Serializable form of a chat message
 * FIXME: not tested
 */
public class Message implements Serializable {

	private static final long serialVersionUID = -711571779132624479L;
	
	/**
	 * the contents of the message
	 */
	public final String text;
	
	/**
	 * whether the message is from the user
	 */
	public final boolean fromUser;

	/**
	 * the constructor for a new message object
	 * @param text the text contained in the message
	 * @param user whether the user has written this message
	 */
	public Message(String text, boolean user) {
		this.text = text;
		this.fromUser = user;
	}

}

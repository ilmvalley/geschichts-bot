package geschichts_bot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import foxLog.threaded.AsyncLog;

/**
 * a class handling a property store file
 * FIXME: not covered by tests
 */
public class SingleInstanceFile {
	/**
	 * a property storage containing all the values
	 */
	private static Properties storage = new Properties();
	/**
	 * the file the property storage is saved in
	 */
	private static File storageFile = new File(Main.saveFolder.getPath() + '/' + "runningStore");
	
	/**
	 * load the properties from the file
	 */
	public static synchronized void load() { 
		//if it's there
		if (!storageFile.exists())
			return;
		//try to create and use the input stream
		try (FileInputStream fin = new FileInputStream(storageFile)) {
			//load the properties
			storage.load(fin);
		} catch (IOException e) {
			//oh no something went wrong
			AsyncLog.error("couldn't load properties file: "+storageFile.getName(), e);
		}
	}

	/**
	 * save the properties to the file
	 */
	public static synchronized void save() {
		//remove the file
		if (storageFile.exists()) {
			storageFile.delete();
		}
		try {
			storageFile.createNewFile();
		} catch (IOException e) {
			AsyncLog.error("couldn't create single instance file: "+storageFile.getName(), e);
		}
		//try to create and use the output stream
		try (FileOutputStream fout = new FileOutputStream(storageFile)) {
			//save the properties
			storage.store(fout,"geschichts-bot instance communication file");
		} catch (IOException e) {
			//something went wrong
			AsyncLog.error("couldn't save properties to file: "+storageFile.getName(), e);
		}
	}

	/**
	 * get a property
	 * @param key
	 *   the property's name
	 * @return
	 *   the property's value or null if not present
	 */
	public static String get(String key) {
		return storage.getProperty(key);
	}
	
	/**
	 * get a property
	 * @param key
	 *   the property's name
	 * @param defaultValue
	 *   the value to be returned if the property is not present
	 * @return
	 *  the property's value or the default
	 */
	public static String get(String key, String defaultValue) {
		String prop = storage.getProperty(key);
		if (prop==null) {
			return defaultValue;
		}
		return prop;
	}

	/**
	 * set a property
	 * @param key
	 *   the property's name
	 * @param value
	 *   the property's new value
	 */
	public static void set(String key, String value) {
		storage.setProperty(key, value);
	}

	public static void delete() {
		AsyncLog.warning("deleting Storage");
		storageFile.delete();
	}

}
